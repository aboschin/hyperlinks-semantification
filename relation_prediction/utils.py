import numpy as np
from collections import defaultdict
import pickle
from scipy import sparse
from torch import empty, cat
from torchkge.exceptions import NotYetEvaluatedError
from torchkge.utils import DataLoader, get_rank
from tqdm.autonotebook import tqdm


def get_biadj(kg):
    # all triplets from wikidata-vitals (entities and attributes)
    heads = np.array([kg.ix2ent[h] for h in kg.head_idx.numpy()])
    tails = np.array([kg.ix2ent[t] for t in kg.tail_idx.numpy()])
    relations = np.array([kg.ix2rel[r] for r in kg.relations.numpy()])

    # keep only triplets giving 'isInstance' relations of a core entity
    mask = np.in1d(heads, list(kg.ent2ix.keys())) & (relations == 'P31')
    heads = heads[mask]
    tails = tails[mask]

    types = np.unique(tails)

    typed_entities = np.unique(heads)
    typedents2index = {ent: i for i, ent in enumerate(typed_entities)}
    index2typedents = {v: k for k, v in typedents2index.items()}

    types2ix = {type_: i for i, type_ in enumerate(types)}

    from_ = [typedents2index[ent] for ent in heads]
    to_ = [types2ix[type_] for type_ in tails]

    biadj = sparse.csr_matrix((np.ones_like(from_), (from_, to_)), shape=(len(typed_entities), len(types)))

    return biadj, index2typedents


def wikidata_natural_typing(kg_tr):
    biadj, ix2typedid = get_biadj(kg_tr)

    clusters = np.array(range(biadj.shape[1]))

    id2cluster = defaultdict(set)
    for ent_ix, type_ix in zip(biadj.nonzero()[0], biadj.nonzero()[1]):
        id2cluster[ix2typedid[ent_ix]].add(type_ix)

    id2cluster = dict(id2cluster)

    ix2ent = {v: k for k, v in kg_tr.ent2ix.items()}
    ix2rel = {v: k for k, v in kg_tr.rel2ix.items()}

    # matrix[i, j] contient la liste des relations allant du cluster i au cluster j
    matrix = get_type_matrix(kg_tr, id2cluster, ix2ent, ix2rel)

    return id2cluster, matrix, clusters


def fb15k237_natural_typing(kg_tr):
    with open('../data/fb2wdtyping.pkl', 'rb') as f:
        id2types, types, id2fb = pickle.load(f)

    typedid2ix = {k: i for i, k in enumerate(id2types.keys())}
    ix2typedid = {v: k for k, v in typedid2ix.items()}

    types2ix = {label: i for i, label in enumerate(types)}

    from_ = [typedid2ix[k] for k, v in id2types.items() for _ in v]
    to_ = [types2ix[t] for _, v in id2types.items() for t in v]

    biadj = sparse.csr_matrix((np.ones_like(from_), (from_, to_)),
                              shape=(len(id2types), len(types)))

    id2clusters = defaultdict(set)
    for ent_ix, type_ix in zip(biadj.nonzero()[0], biadj.nonzero()[1]):
        id2clusters[ix2typedid[ent_ix]].add(type_ix)

    fb2clusters = {id2fb[id_]: clusters for id_, clusters in id2clusters.items()}

    ix2ent = {v: k for k, v in kg_tr.ent2ix.items()}
    ix2rel = {v: k for k, v in kg_tr.rel2ix.items()}

    # matrix[i, j] contient la liste des relations allant du cluster i au cluster j
    matrix = get_type_matrix(kg_tr, fb2clusters, ix2ent, ix2rel)

    return fb2clusters, matrix


def get_type_matrix(kg_tr, id2cluster, ix2ent, ix2rel):
    # matrix[i, j] contient la liste des relations allant du cluster i au cluster j
    matrix = defaultdict(set)
    for h, t, r in kg_tr:
        try:
            for i in id2cluster[ix2ent[h]]:
                for j in id2cluster[ix2ent[t]]:
                    matrix[(i, j)].add(ix2rel[r])
        except:
            pass
    return matrix


def get_ix2cluster(ent2ix, rel2ix, id2cluster, matrix):
    ix2cluster = {ent2ix[ent]: cluster for ent, cluster in
                  id2cluster.items()}

    matrix = dict(matrix)
    for k, v in matrix.items():
        matrix[k] = [rel2ix[rel] for rel in v]
    return ix2cluster, matrix


def get_dictionaries(kg, kg_te=None):
    dict_of_rels = defaultdict(set)
    for i in range(kg.n_facts):
        dict_of_rels[(kg.head_idx[i].item(), kg.tail_idx[i].item())].add(kg.relations[i].item())
    if kg_te is not None:
        for i in range(kg_te.n_facts):
            dict_of_rels[(kg_te.head_idx[i].item(), kg_te.tail_idx[i].item())].add(kg_te.relations[i].item())
    return dict_of_rels


class RelationPredictionEvaluator(object):
    """ Class highly inspired from the TorchKGE.evaluation.LinkPredictionEvaluator class but adapted for the
    Relation-Prediction task.

    """

    def __init__(self, model, knowledge_graph, directed=True):
        self.model = model
        self.kg = knowledge_graph

        self.rank_true_rels = empty(size=(knowledge_graph.n_facts,)).long()
        self.filt_rank_true_rels = empty(size=(knowledge_graph.n_facts,)).long()

        self.directed = directed

        if self.directed:
            self.scores = empty(size=(knowledge_graph.n_facts, knowledge_graph.n_rel))
            self.filt_scores = empty(size=(knowledge_graph.n_facts, knowledge_graph.n_rel))
        else:
            self.scores = empty(size=(knowledge_graph.n_facts, 2*knowledge_graph.n_rel))
            self.filt_scores = empty(size=(knowledge_graph.n_facts, 2*knowledge_graph.n_rel))

        self.evaluated = False

    def evaluate(self, b_size, verbose=True):
        use_cuda = next(self.model.parameters()).is_cuda

        if use_cuda:
            dataloader = DataLoader(self.kg, batch_size=b_size, use_cuda='batch')
            self.rank_true_rels = self.rank_true_rels.cuda()
            self.filt_rank_true_rels = self.filt_rank_true_rels.cuda()

        else:
            dataloader = DataLoader(self.kg, batch_size=b_size)

        for i, batch in tqdm(enumerate(dataloader), total=len(dataloader),
                             unit='batch', disable=(not verbose),
                             desc='Relation prediction evaluation'):
            h_idx, t_idx, r_idx = batch[0], batch[1], batch[2]

            if self.directed:
                rk_true_r, f_rk_true_r, scores, filt_scores = self.model.lp_helper(h_idx, t_idx, r_idx, self.kg,
                                                                                   self.directed)

            else:
                _, _, scores1, filt_scores1 = self.model.lp_helper(h_idx, t_idx, r_idx, self.kg, self.directed)
                _, _, scores2, filt_scores2 = self.model.lp_helper(t_idx, h_idx, r_idx, self.kg, self.directed)

                scores = cat((scores1, scores2), dim=1)
                filt_scores = cat((filt_scores1, filt_scores2), dim=1)

                rk_true_r, f_rk_true_r = get_rank(scores, r_idx), get_rank(filt_scores, r_idx)

            self.rank_true_rels[i * b_size: (i + 1) * b_size] = rk_true_r.detach()
            self.filt_rank_true_rels[i * b_size: (i + 1) * b_size] = f_rk_true_r.detach()
            self.scores[i * b_size: (i + 1) * b_size, :] = scores.detach()
            self.filt_scores[i * b_size: (i + 1) * b_size, :] = filt_scores.detach()

        self.evaluated = True

        if use_cuda:
            self.rank_true_rels = self.rank_true_rels.cpu()
            self.filt_rank_true_rels = self.filt_rank_true_rels.cpu()
            self.scores = self.scores.cpu()
            self.filt_scores = self.filt_scores.cpu()

    def mean_rank(self):
        if not self.evaluated:
            raise NotYetEvaluatedError('Evaluator not evaluated call '
                                       'LinkPredictionEvaluator.evaluate')
        sum_ = self.rank_true_rels.float().mean().item()
        filt_sum = self.filt_rank_true_rels.float().mean().item()
        return sum_, filt_sum

    def hit_at_k(self, k=10):
        if not self.evaluated:
            raise NotYetEvaluatedError('Evaluator not evaluated call '
                                       'LinkPredictionEvaluator.evaluate')

        rel_hit = (self.rank_true_rels <= k).float().mean().item()
        filt_rel_hit = (self.filt_rank_true_rels <= k).float().mean().item()

        return rel_hit, filt_rel_hit

    def mrr(self):
        if not self.evaluated:
            raise NotYetEvaluatedError('Evaluator not evaluated call '
                                       'LinkPredictionEvaluator.evaluate')
        rel_mrr = (self.rank_true_rels.float() ** (-1)).mean()
        filt_rel_mrr = (self.filt_rank_true_rels.float() ** (-1)).mean()

        return rel_mrr.item(), filt_rel_mrr.item()

    def print_results(self, k=None, n_digits=3):
        if k is None:
            k = 10

        if k is not None and type(k) == int:
            print('Hit@{} : {} \t\t Filt. Hit@{} : {}'.format(
                k, round(self.hit_at_k(k=k)[0], n_digits),
                k, round(self.hit_at_k(k=k)[1], n_digits)))
        if k is not None and type(k) == list:
            for i in k:
                print('Hit@{} : {} \t\t Filt. Hit@{} : {}'.format(
                    i, round(self.hit_at_k(k=i)[0], n_digits),
                    i, round(self.hit_at_k(k=i)[1], n_digits)))

        print('Mean Rank : {} \t Filt. Mean Rank : {}'.format(
            round(self.mean_rank()[0], n_digits), round(self.mean_rank()[1], n_digits)))
        print('MRR : {} \t\t Filt. MRR : {}'.format(
            round(self.mrr()[0], n_digits), round(self.mrr()[1], n_digits)))