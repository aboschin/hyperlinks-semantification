from collections import defaultdict
import numpy as np
import pickle
from torchkge.utils import load_fb15k237
from tqdm.autonotebook import tqdm
from utils import get_ix2cluster
from utils import wikidata_natural_typing, fb15k237_natural_typing

dataset_name = 'WDV5'
data_path = None  # should point to the `WDV5_split.pkl` file written by the training procedure

########################################################################
if dataset_name == 'WDV5' and data_path is not None:
    with open(data_path, 'rb') as f:
        kg_tr, kg_val, kg_te = pickle.load(f)
else:
    kg_tr, kg_val, kg_te = load_fb15k237()

print('Loaded: dataset {}.'.format(dataset_name))

kg_tr.ix2ent = {v: k for (k, v) in kg_tr.ent2ix.items()}
kg_tr.ix2rel = {v: k for (k, v) in kg_tr.rel2ix.items()}

########################################################################
if dataset_name == 'WDV5':
    id2type, matrix, types = wikidata_natural_typing(kg_tr)
    ix2type, _ = get_ix2cluster(kg_tr.ent2ix, kg_tr.rel2ix, id2type, matrix)
else:
    assert dataset_name == 'FB15k237'
    fb2types, matrix = fb15k237_natural_typing(kg_tr)
    ix2type, _ = get_ix2cluster(kg_tr.ent2ix, kg_tr.rel2ix, fb2types, matrix)

matrix = defaultdict(list)
popularity_singles = [0 for _ in range(kg_tr.n_rel)]

for h, t, r in tqdm(kg_tr):
    popularity_singles[r] += 1
    try:
        for type_h in ix2type[h]:
            for type_t in ix2type[t]:
                matrix[(type_h, type_t)].append(r)
    except KeyError:
        pass

popularity_singles = np.argsort(-np.array(popularity_singles))
for k, v in matrix.items():
    d = {}
    for val in np.unique(v):
        d[val] = (np.array(v) == val).sum()
    matrix[k] = d

ranks = []
for h, t, r in kg_te:
    try:
        types_h = ix2type[h]
        types_t = ix2type[t]
        for type_h in types_h:
            for type_t in types_t:
                if type(matrix[(type_h, type_t)]) != list:
                    candidates = np.array([k for k, v in sorted(
                        matrix[(type_h, type_t)].items(),
                        key=lambda item: item[1], reverse=True)])
                    if (candidates == r).any():
                        ranks.append((candidates == r).argmax())
                    else:
                        ranks.append((popularity_singles == r).argmax())
                else:
                    ranks.append((popularity_singles == r).argmax())
    except KeyError:
        ranks.append((popularity_singles == r).argmax())
ranks = np.array(ranks) + 1

print('MRR: {}'.format((1 / np.array(ranks)).mean()))
print('Hit@1: {}'.format((np.array(ranks) < 2).mean()))
print('Hit@5: {}'.format((np.array(ranks) < 6).mean()))
