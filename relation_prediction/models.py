from torch import tensor
from torchkge.models import TransEModel, ComplExModel
from torchkge.utils import get_rank


class TransERels(TransEModel):
    """Class based on the TorchKGE one but with Type Filtering added for Relation Prediction.

    """
    def __init__(self, emb_dim, ix2cluster, kg_tr, matrix, type_filtering=True):
        super().__init__(emb_dim, kg_tr.n_ent, kg_tr.n_rel)
        self.ix2cluster = ix2cluster
        self.kg_tr = kg_tr
        self.matrix = matrix
        self.type_filtering = type_filtering

    def lp_prep_cands(self, h_idx, t_idx, r_idx):
        b_size = h_idx.shape[0]

        h_emb = self.ent_emb(h_idx)
        t_emb = self.ent_emb(t_idx)

        candidates = self.rel_emb.weight.data.view(1, self.n_rel, self.emb_dim)
        candidates = candidates.expand(b_size, self.n_rel, self.emb_dim)

        return h_emb, t_emb, candidates

    def lp_scoring_function(self, proj_h, proj_t, r):
        b_size, emb_dim = proj_h.shape[0], proj_h.shape[1]

        h_ = proj_h.view(b_size, 1, emb_dim)
        t_ = proj_t.view(b_size, 1, emb_dim)

        return - self.dissimilarity(h_ + r, t_)

    def lp_compute_ranks(self, h_emb, t_emb, candidates, h_idx, t_idx, r_idx, dictionary, directed):
        b_size = r_idx.shape[0]

        scores = self.lp_scoring_function(h_emb, t_emb, candidates)

        if self.type_filtering:
            for i in range(b_size):
                possible_relations = []
                try:
                    for h_cluster in self.ix2cluster[h_idx[i].item()]:
                        for t_cluster in self.ix2cluster[t_idx[i].item()]:
                            try:
                                possible_relations.append(set(self.matrix[(h_cluster, t_cluster)]))
                            except KeyError:
                                pass
                    possible_relations = list(set.intersection(*possible_relations))
                    possible_relations = tensor(possible_relations).long()
                except TypeError:
                    assert len(possible_relations) == 0
                    possible_relations = self.kg_tr.relations[(self.kg_tr.head_idx == h_idx[i].cpu()) |
                                                              (self.kg_tr.tail_idx == t_idx[i].cpu())].unique()
                except KeyError:
                    possible_relations = self.kg_tr.relations[(self.kg_tr.head_idx == h_idx[i].cpu()) |
                                                              (self.kg_tr.tail_idx == t_idx[i].cpu())].unique()
                if len(possible_relations) > 0:
                    tmp = tensor([True for _ in range(self.kg_tr.n_rel)])
                    tmp[possible_relations] = False
                    scores[i][tmp] = - float('Inf')

        # filter out the true negative samples by assigning - inf score.
        filt_scores = scores.clone()
        for i in range(b_size):
            true_targets = get_true_targets(dictionary, h_idx, t_idx, r_idx, i, directed)
            if true_targets is None:
                continue
            filt_scores[i][true_targets] = - float('Inf')

        # from dissimilarities, extract the rank of the true entity.
        rank_true_entities = get_rank(scores, r_idx)
        filtered_rank_true_entities = get_rank(filt_scores, r_idx)

        return rank_true_entities, filtered_rank_true_entities, scores, filt_scores

    def lp_helper(self, h_idx, t_idx, r_idx, kg, directed):
        h_emb, t_emb, candidates = self.lp_prep_cands(h_idx, t_idx, r_idx)

        rank_true_rels, filt_rank_true_rels, scores, filt_scores = self.lp_compute_ranks(h_emb, t_emb, candidates,
                                                                                         h_idx, t_idx, r_idx,
                                                                                         kg.dict_of_rels, directed)

        return rank_true_rels, filt_rank_true_rels, scores, filt_scores


class ComplExRels(ComplExModel):
    """Class based on the TorchKGE one but with Type Filtering added for Relation Prediction.

    """
    def __init__(self, emb_dim, ix2cluster, kg_tr, matrix, type_filtering=True):
        super().__init__(emb_dim, kg_tr.n_ent, kg_tr.n_rel)
        self.ix2cluster = ix2cluster
        self.kg_tr = kg_tr
        self.matrix = matrix
        self.type_filtering = type_filtering

    def lp_prep_cands(self, h_idx, t_idx, r_idx):
        b_size = h_idx.shape[0]

        re_h, im_h = self.re_ent_emb(h_idx), self.im_ent_emb(h_idx)
        re_t, im_t = self.re_ent_emb(t_idx), self.im_ent_emb(t_idx)

        re_candidates = self.re_rel_emb.weight.data.view(1, self.n_rel, self.emb_dim)
        re_candidates = re_candidates.expand(b_size, self.n_rel, self.emb_dim)

        im_candidates = self.im_rel_emb.weight.data.view(1, self.n_rel, self.emb_dim)
        im_candidates = im_candidates.expand(b_size, self.n_rel, self.emb_dim)

        return (re_h, im_h), (re_t, im_t), (re_candidates, im_candidates)

    def lp_scoring_function(self, proj_h, proj_t, r):
        re_h, im_h = proj_h[0], proj_h[1]
        re_t, im_t = proj_t[0], proj_t[1]
        re_r, im_r = r[0], r[1]
        b_size, emb_dim = re_h.shape[0], re_h.shape[1]

        re_h = re_h.view(b_size, self.emb_dim, 1)
        re_t = re_t.view(b_size, self.emb_dim, 1)
        im_h = im_h.view(b_size, self.emb_dim, 1)
        im_t = im_t.view(b_size, self.emb_dim, 1)

        re_r = re_r.transpose(1, 2)
        im_r = im_r.transpose(1, 2)

        return (re_h * re_r * re_t + re_h * im_r * im_t + im_h * re_r * im_t - im_h * im_r * re_t).sum(dim=1)

    def lp_compute_ranks(self, h_emb, t_emb, candidates, h_idx, t_idx, r_idx, dictionary, directed):
        b_size = r_idx.shape[0]

        scores = self.lp_scoring_function(h_emb, t_emb, candidates)

        if self.type_filtering:
            for i in range(b_size):
                possible_relations = []
                try:
                    for h_cluster in self.ix2cluster[h_idx[i].item()]:
                        for t_cluster in self.ix2cluster[t_idx[i].item()]:
                            try:
                                possible_relations.append(set(self.matrix[(h_cluster, t_cluster)]))
                            except KeyError:
                                pass
                    possible_relations = list(set.intersection(*possible_relations))
                    possible_relations = tensor(possible_relations).long()
                except TypeError:
                    assert len(possible_relations) == 0
                    possible_relations = self.kg_tr.relations[(self.kg_tr.head_idx == h_idx[i].cpu()) |
                                                              (self.kg_tr.tail_idx == t_idx[i].cpu())].unique()
                except KeyError:
                    possible_relations = self.kg_tr.relations[(self.kg_tr.head_idx == h_idx[i].cpu()) |
                                                              (self.kg_tr.tail_idx == t_idx[i].cpu())].unique()
                if len(possible_relations) > 0:
                    tmp = tensor([True for _ in range(self.kg_tr.n_rel)])
                    tmp[possible_relations] = False
                    scores[i][tmp] = - float('Inf')

        # filter out the true negative samples by assigning - inf score.
        filt_scores = scores.clone()
        for i in range(b_size):
            true_targets = get_true_targets(dictionary, h_idx, t_idx, r_idx, i, directed)
            if true_targets is None:
                continue
            filt_scores[i][true_targets] = - float('Inf')

        # from dissimilarities, extract the rank of the true entity.
        rank_true_entities = get_rank(scores, r_idx)
        filtered_rank_true_entities = get_rank(filt_scores, r_idx)

        return rank_true_entities, filtered_rank_true_entities, scores, filt_scores

    def lp_helper(self, h_idx, t_idx, r_idx, kg, directed):
        h_emb, t_emb, candidates = self.lp_prep_cands(h_idx, t_idx, r_idx)

        rank_true_rels, filt_rank_true_rels, scores, filt_scores = self.lp_compute_ranks(
            h_emb, t_emb, candidates, h_idx, t_idx, r_idx, kg.dict_of_rels, directed)

        return rank_true_rels, filt_rank_true_rels, scores, filt_scores


def get_true_targets(dictionary, e_idx, r_idx, true_idx, i, directed):
    true_targets = dictionary[e_idx[i].item(), r_idx[i].item()].copy()
    if directed:
        if len(true_targets) == 1:
            return None
        true_targets.remove(true_idx[i].item())
    else:
        if len(true_targets) <= 1:
            return None
        try:
            true_targets.remove(true_idx[i].item())
        except KeyError:
            pass
    return tensor(list(true_targets)).long()
