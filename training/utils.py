import pickle
from torchkge.sampling import NegativeSampler
from torchkge.utils import get_bernoulli_probs
from torch import tensor, bernoulli, randint, ones


class BalNS(NegativeSampler):
    """Class based on TorchKGE's Negative Sampler class implementing the BalNS method.

    """

    def __init__(self, kg, kg_val=None, kg_test=None, rel_share=.5):
        super().__init__(kg, kg_val, kg_test, n_neg=1)
        self.bern_probs = self.evaluate_probabilities()
        self.rel_share = rel_share

    def evaluate_probabilities(self):
        bern_probs = get_bernoulli_probs(self.kg)

        tmp = []
        for i in range(self.kg.n_rel):
            if i in bern_probs.keys():
                tmp.append(bern_probs[i])
            else:
                tmp.append(0.5)

        return tensor(tmp).float()

    def corrupt_batch(self, heads, tails, relations, n_neg=None):

        device = heads.device
        assert (device == tails.device)

        batch_size = heads.shape[0]
        neg_heads = heads.clone().detach()
        neg_tails = tails.clone().detach()
        neg_rels = relations.clone().detach()

        mask1 = bernoulli(self.rel_share * ones(batch_size)).double()  # if 1 then entities are corrupted

        neg_rels[mask1 == 0] = randint(1, self.kg.n_rel, (
        int(batch_size - (mask1.sum().item())),), device=device)

        # Randomly choose which samples will have head/tail corrupted
        mask2 = ones(len(mask1))
        mask2[mask1 == 0] = 0.

        mask2[mask2 == 1] = bernoulli(self.bern_probs[relations[mask1 == 1]])

        mask2 = mask2.double()

        n_h_cor = int(mask2.sum().item())
        neg_heads[(mask1 == 1) & (mask2 == 1)] = randint(1, self.n_ent,
                                                         (n_h_cor,),
                                                         device=device)
        neg_tails[(mask1 == 1) & (mask2 == 0)] = randint(1, self.n_ent,
                                                         (int(
                                                             mask1.sum().item()) - n_h_cor,),
                                                         device=device)

        return neg_heads.long(), neg_tails.long(), neg_rels.long()


def load_WDV5(path):
    with open(path, 'rb') as f:
        kg, kg_attr = pickle.load(f)
    return kg, kg_attr


def write_state_dict(state_dict, export_name, mrr, data_path):
    state_dict = dict(state_dict)
    for k, v in state_dict.items():
        state_dict[k] = v.detach().clone().cpu()
    with open(data_path + 'training_output/{}_best_model_{}.pkl'.format(export_name, mrr), 'wb') as f:
        pickle.dump(state_dict, f)


def print_save_current_results(best_mrr, test_mrr, test_hat10, epoch, lr, batch_size, emb_dim, loss_group,
                               lr_decay, weight_decay, dropout, export_name, data_path):

    if loss_group['loss_type'] == 'margin':
        result_string = 'val_mrr {}, test_mrr {}, test_h@10 {}, epoch {}, lr {}, bs {}, dim {}, loss {}, margin {}, lr_decay {}, l2reg {}, dropout {}'.format(
                round(best_mrr, 4), round(test_mrr, 4), round(test_hat10, 4), epoch, lr, batch_size, emb_dim,
                loss_group['loss_type'], loss_group['margin'], lr_decay, weight_decay,
                dropout)
        key = (lr, batch_size, emb_dim, loss_group['loss_type'], loss_group['margin'],
               lr_decay, weight_decay, dropout)
        value = (best_mrr, epoch)

    else:
        result_string = 'val_mrr {}, test_mrr {}, test_h@10 {}, epoch {}, lr {}, bs {}, dim {}, loss {}, lr_decay {}, l2reg {}, dropout {}'.format(
                round(best_mrr, 4), round(test_mrr, 4), round(test_hat10, 4), epoch, lr, batch_size, emb_dim,
                loss_group['loss_type'], lr_decay, weight_decay, dropout)
        key = (lr, batch_size, emb_dim, loss_group['loss_type'],
               lr_decay, weight_decay, dropout)
        value = (best_mrr, epoch)

    print(result_string)

    with open(data_path + 'training_output/{}.txt'.format(export_name), 'a') as f:
        f.write(result_string + '\n')

    return key, value