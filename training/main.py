from utils import BalNS, load_WDV5, write_state_dict, print_save_current_results
from models import TransEPlus, ComplExPlus

from hyperopt import fmin, hp, tpe
from ignite.engine import Engine, Events
from ignite.handlers import EarlyStopping
from ignite.metrics import RunningAverage
import pickle

import torch
from torch.optim import Adam
from torch.optim.lr_scheduler import ExponentialLR
from torchkge.evaluation import LinkPredictionEvaluator
from torchkge.utils import load_fb15k237, load_wikidata_vitals, DataLoader
from torchkge.utils import BinaryCrossEntropyLoss, LogisticLoss, MarginLoss
from torchkge.sampling import BernoulliNegativeSampler as BerNS

model_name = 'TransE'  # or 'ComplEx'
NS_type = 'BalNS'  # or 'BerNS'
dataset = 'WDV5'  # or 'FB15k237'

cuda_device_number = 0  # Number of the GPU to be used if available
evaluation_bsize = 10  # Depends of the available memory on the specified GPU
data_path = '../data/'

eval_epoch = 20  # Period of evaluation by Link-Prediction
max_epochs = 3000
patience = 100  # For the early stopping (if no progress in that much epochs then training is stopped)
number_iterations = 600  # Max number of hyper-parameters combinations to be tried.

# Define the space search for the hyper-parameters of the model
space = {'lr': hp.choice('lr', [0.001, 0.005, 0.01, 0.05]),
         'batch_size': hp.choice('batch_size', [4096, 8192, 16384, 32768, 65536]),
         'emb_dim': hp.choice('emb_dim', [100, 150, 200]),
         'weight_decay': hp.choice('weight_decay', [1e-5, 5e-5, 1e-4]),
         'lr_decay': hp.choice('lr_decay', [1., 0.995, 0.99, 0.985]),
         'dropout': hp.choice('dropout', [0, 0.1, 0.2, 0.3, 0.4, 0.5]),
         'loss_group': hp.choice('loss_group', [{'loss_type': 'bce'},
                                                {'loss_type': 'logistic'},
                                                {'loss_type': 'margin',
                                                 'margin': hp.choice('margin',
                                                                     [0.5, 1,
                                                                      2, 3, 10])}
                                                ])
         }

assert model_name in ['TransE', 'ComplEx']
assert NS_type in ['BerNS', 'BalNS']
assert dataset in ['WDV5', 'FB15k237']

if dataset == 'WDV5':
    # create a split
    kg, _ = load_wikidata_vitals(5)

    kg_tr, kg_val, kg_te = kg.split_kg(share=0.8, validation=True)
    # save it as pickle file
    with open(data_path + 'training_output/WDV5_split.pkl', 'wb') as f:
        pickle.dump((kg_tr, kg_val, kg_te), f)

else:
    kg_tr, kg_val, kg_te = load_fb15k237()

if torch.cuda.is_available():
    torch.cuda.set_device(cuda_device_number)
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

export_name = '{}_{}_{}'.format(model_name, dataset, NS_type)


def process_batch(engine, batch):
    model.train()
    # get the input
    heads, tails, rels = batch[0], batch[1], batch[2]

    # zero model gradient
    model.zero_grad()

    # Create Negative Samples
    if NS_type == 'BerNS':
        neg_heads, neg_tails = sampler.corrupt_batch(heads, tails, rels)
        output = model(heads, tails, neg_heads, neg_tails, rels, rels)
    else:
        neg_heads, neg_tails, neg_rels = sampler.corrupt_batch(heads, tails, rels)
        output = model(heads, tails, neg_heads, neg_tails, rels, neg_rels)

    loss = criterion(output[0], output[1])
    loss.backward()
    optimizer.step()

    return loss.item()


def linkprediction_evaluation(engine):
    model.normalize_parameters()
    model.eval()
    scheduler.step()

    # validation hit@k measure
    if engine.state.epoch % eval_epoch == 0:
        evaluator = LinkPredictionEvaluator(model, kg_val)
        evaluator.evaluate(b_size=evaluation_bsize, verbose=False)
        val_mrr = evaluator.mrr()[1]
    else:
        val_mrr = 0

    if val_mrr >= 1.:
        val_mrr = 0

    try:
        if engine.state.best_mrr < val_mrr:
            engine.state.best_mrr = val_mrr
            tmp = dict(model.state_dict())
            engine.state.best_parameters = {k: tmp[k].clone() for k in tmp}

        return val_mrr

    except AttributeError as e:
        if engine.state.epoch == 1:
            engine.state.best_mrr = val_mrr
            tmp = dict(model.state_dict())
            engine.state.best_parameters = {k: tmp[k].clone() for k in tmp}
            return val_mrr
        else:
            raise e


def test(kg_train, kg_val, kg_test, emb_dim, lr, max_epochs, patience,
         batch_size, loss_group, lr_decay, weight_decay, dropout, verbose=False):

    n_ent, n_rel = kg_train.n_ent, kg_train.n_rel

    global model, optimizer, criterion, sampler, scheduler
    try:
        del model, optimizer, criterion, sampler
        # print('Global variables deleted')
    except NameError:
        pass

    # Define the model, optimizer and criterion
    if model_name == 'TransE':
        model = TransEPlus(emb_dim, n_ent, n_rel, input_dropout_rate=dropout)
    else:
        model = ComplExPlus(emb_dim, n_ent, n_rel, input_dropout_rate=dropout)
    model.to(device)

    optimizer = Adam(model.parameters(), lr=lr, weight_decay=weight_decay)
    scheduler = ExponentialLR(optimizer, lr_decay)

    if loss_group['loss_type'] == 'bce':
        criterion = BinaryCrossEntropyLoss()
    elif loss_group['loss_type'] == 'logistic':
        criterion = LogisticLoss()
    else:
        assert loss_group['loss_type'] == 'margin'
        criterion = MarginLoss(loss_group['margin'])

    if NS_type == 'BerNS':
        sampler = BerNS(kg_train, kg_val=kg_val, kg_test=kg_test)
    else:
        sampler = BalNS(kg_train, kg_val=kg_val, kg_test=kg_test, rel_share=0.5)

    # Define the engine
    trainer = Engine(process_batch)

    # Define the moving average
    RunningAverage(output_transform=lambda x: x).attach(trainer, 'margin')

    # Add early stopping
    handler = EarlyStopping(patience=patience,
                            score_function=linkprediction_evaluation,
                            trainer=trainer)
    trainer.add_event_handler(Events.EPOCH_COMPLETED, handler)

    # Define the iterator for training
    train_iterator = DataLoader(kg_train, batch_size, use_cuda='all')

    trainer.run(train_iterator, epoch_length=len(train_iterator), max_epochs=max_epochs)

    if verbose:
        print('Best score {:.3f} at epoch {}'.format(handler.best_score,
                                                     trainer.state.epoch - handler.patience))

    model.load_state_dict(trainer.state.best_parameters)
    model.normalize_parameters()
    model.eval()

    evaluator = LinkPredictionEvaluator(model, kg_test)
    evaluator.evaluate(b_size=evaluation_bsize, verbose=False)
    test_mrr, test_hat10 = evaluator.mrr()[1], evaluator.hit_at_k(10)[1]

    if test_mrr >= 1:
        test_mrr = 0
        test_hat10 = 0

    state_dict = dict(model.state_dict())

    return handler.best_score, trainer.state.epoch - handler.patience, test_mrr, test_hat10, state_dict


def f(space):
    lr = space['lr']
    batch_size = space['batch_size']
    emb_dim = space['emb_dim']
    loss_group = space['loss_group']
    lr_decay = space['lr_decay']
    weight_decay = space['weight_decay']
    dropout = space['dropout']

    best_mrr, epoch, test_mrr, test_hat10, state_dict = test(kg_tr, kg_val, kg_te, emb_dim, lr, max_epochs,
                                                             patience, batch_size, loss_group, lr_decay, weight_decay,
                                                             dropout)

    key, value = print_save_current_results(best_mrr, test_mrr, test_hat10, epoch, lr, batch_size, emb_dim,
                                            loss_group, lr_decay, weight_decay, dropout, export_name, data_path)
    result_dict[key] = value
    global best_score
    if best_mrr > best_score:
        best_score = best_mrr
        write_state_dict(state_dict, export_name, best_mrr, data_path)

    return -best_mrr


global result_dict
result_dict = {}

global best_score
best_score = -1

best = fmin(fn=f, space=space, algo=tpe.suggest, max_evals=number_iterations,
            return_argmin=False)

with open(data_path + 'training_output/{}_details.pkl'.format(export_name), 'wb') as f:
    pickle.dump((best, result_dict), f)