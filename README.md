# Relation Prediction by Knowledge Graph Embedding

Remarks:
* The datasets Wikivitals+ mentioned in the article is provided in `data/`
* In the `data/pretrained_models` directory are provided a pretrained version of TransE for FB15k237 and for WDV5 (along with the corresponding split of WDV5). Running `training/main.py` without changing the values of the variables (the paths) might rewrite those. 


## Requirements

Python 3.8 should be installed allong with the following Python libraries:
* hyperopt
* pandas
* numpy
* pandas
* pickle
* pytorch-ignite == 0.4.4
* scipy
* torch == 1.7.0
* torchkge == 0.16.25
* tqdm

The directory is organized as follows:
* `data` 
    * `wikivitals+.csv` list of hyperlinks from Wikipedia: this is a subset of the [Wikivitals](https://netset.telecom-paris.fr/pages/wikivitals+.html) dataset, keeping only the pages that have a corresponding Wikidata entity.
    * `fb2wdtyping.pkl` files containing the Wikidata types of FB15k237 entities.
    * `data/training_output` this directory should receive the output of the `training/main.py` script.
    * `data/pretrained_models`
        * `TransE_WDV5_BalNS.pkl` the state dictionary of a TransE model trained with BalNS on the WDV5 dataset (for the split providedin the same directory)
        * `TransE_FB15k237_BalNS.pkl` the state dictionary of a TransE model trained with BalNS on the FB15k237 dataset.
* `relation_prediction`
    * `baseline.py` script to evaluate the baseline model using only Type Filtering and ranking by popularity. The `data_path` variable should point to the `WDV5_split.pkl` file written during training.
    * `evaluate.py` script to evaluate a trained model (which state dictionary is stored as a pickle file) on Relation-Prediction. The `data_path` variable should point to the `WDV5_split.pkl` file written during training. 
    * `model.py` where the classes of the models are defined with methods specific to Relation-Prediction
    * `utils.py` contains utility functions and classes
* `training`
    * `main.py` script to search for hyper-parameters by grid-search or to train a model (by reducing the search space to only one combination) 
    * `models.py` where the classes of the models are defined with specific methods required for BalNS.
    * `utils.py` contains utility functions and classes
    * `wikipedia_application.ipynb` Jupyter notebook for application of the Relation-Prediction to Wikipedia Hyperlinks

## Grid Search to find hyper-parameters
Call the script `training/main.py` after defining the values of the variables `model_name` (TransE or ComplEx), `NS_type` (BalNS 
or BerNS) and `dataset` (FB15k237 or WDV5). This runs a grid search and pickles of the model's state dictionnary are dumped each time
there is an improvement in the Link-Prediction MRR score.

## Train a model with known hyper-parameters
Call the script `training/main.py` after defining the values of the variables and also reducing the space search to
the strict minimun (only including the chosen parameters). Choosing a value of 10 for `number_iterations` runs the training 
10 times and dumps pickle files containing the model's state dict when the scores are improved from one run to the other.

## Evaluate a trained model on the Relation-Prediction task
Call the script `relation_prediction/evaluate.py` after specifying the values of the variables `dataset_name`, (`data_path` if the previous is WDV5), 
`model_name`, `model_pkl_path`. The script loads the dataset and the pretrained model, computes and prints the evaluation metrics.

## Apply a trained model to classify Wikipedia Hyperlinks
See the Jupyer notebook `relation_prediction/wikipedia_application.ipynb` for this application.


 